import os

import copy

class Snapshot:
   
    #Shimmer data
    acceleration = []
    
    # Zephyr data
    heart_rates = []
    breathing_rates = []
    skin_temperatures_z = []
    rr_intervals = []
    
    # Bodymedia data
    near_body_temperatures = []
    skin_temperatures_bm = []
    galvanic_skin_response = []
    # calories = []
    # mets_b = []
    
    #Cosmed data
    # mets_c = []
    
    #Smartphone data
    android_acceleration = []
  
    # Activities
    activities = []
    
    # Times
    times = []
    
    average_values = {}
    max_values = {}
    min_values = {}

    def __init__(self, time, snapshot):
        self.time = time
        #if snapshot != None:
            # self.snapshot = self.deepcopy(snapshot)
        #    self.copy_snapshot_and_init(snapshot)
        #else:
        self.init_values()

    def copy_snapshot(self, snapshot):
        snapshot_copy = Snapshot(snapshot.time, snapshot)
        snapshot_copy.acceleration = copy.deepcopy(snapshot.acceleration)
        snapshot_copy.heart_rates = copy.deepcopy(snapshot.heart_rates)
        snapshot_copy.breathing_rates = copy.deepcopy(snapshot.breathing_rates)
        snapshot_copy.skin_temperatures_z = copy.deepcopy(snapshot.skin_temperatures_z)
        snapshot_copy.rr_intervals = copy.deepcopy(snapshot.rr_intervals)
        snapshot_copy.near_body_temperatures = copy.deepcopy(snapshot.near_body_temperatures)
        snapshot_copy.skin_temperatures_bm = copy.deepcopy(snapshot.skin_temperatures_bm)
        snapshot_copy.galvanic_skin_response = copy.deepcopy(snapshot.galvanic_skin_response)
        snapshot_copy.calories = copy.deepcopy(snapshot.calories)
        snapshot_copy.mets_b = copy.deepcopy(snapshot.mets_b)
        snapshot_copy.mets_c = copy.deepcopy(snapshot.mets_c)
        snapshot_copy.android_acceleration = copy.deepcopy(snapshot.android_acceleration)
        snapshot_copy.activities = copy.deepcopy(snapshot.activities)
        snapshot_copy.times = copy.deepcopy(snapshot.times)
        snapshot_copy.scenario = copy.deepcopy(snapshot.scenario)               
    
        return snapshot_copy
        

    def init_values(self):
            self.acceleration = []
            self.heart_rates = []
            self.breathing_rates = []
            self.skin_temperatures_z = []
            self.rr_intervals = []
            self.near_body_temperatures = []
            self.skin_temperatures_bm = []
            self.galvanic_skin_response = []
            self.calories = []
            self.mets_b = []
            self.mets_c = []
            self.android_acceleration = []
            self.activities = []
            self.times = []
            self.scenario = []

  
    def append_acceleration(self, dict_acc):
        self.acceleration.append(dict_acc)
        
    def get_acceleration(self):
        return self.acceleration
        
        
  
 
    