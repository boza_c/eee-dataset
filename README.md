## Energy Expenditure Estimation Dataset ##

The dataset:

* Contains data of 10 healthy volunteers equiped with the following devices
	* **Shimmer sensors:** four Shimmer sensors attached to ankle, thigh, wrist  and chest measuring 3-axial acceleration (ACC) with 50Hz 
	* **Zephyr Biohraness:** measuring heart rate (HR) and breatining rate (BR) with 1 Hz
	* **Bodymedia Fit:** estimating EE and measuring persons galvanic skin response (GSR), Near-Body temperature (NBT), and Skin temperature (ST) once per minute
	* **Android smartphone:** loacted in the users trouser pocket measuring 3-axial acceleration 
	* **COSMED the indirect calorimeter:** mesuring the true expended energy in MET 20 times per minute
* Each person perfromed a predefined set of activities (a scenario) in a controlled environment
* The expended energy is labelled with the indirect calorimenter (Cosmed K4B5 device)

The jupyter notebooks:

* **Energy Expenditure Estimation Dataset Description.ipynb** is used for downloading the dataset and initial exploration of the signals.
* **Dataset Statistics.ipynb** to get an insignt into the dataset stats.
* **Energy Expedniture Estimation.ipynb** for the method (from the raw signal to the machine-learning model)


The dataset was used and published in the following journal paper:

> Božidara Cvetković, Radoje Milić and Mitja Luštrek, **Estimating Energy Expenditure With Multiple Models Using Different Wearable Sensors**, Journal of Biomedical and Health Informatics, vol. 20, no. 4, pp. 1081-1087, 
https://doi.org/10.1109/JBHI.2015.2432911


The methododology was used in the following journal paper:

> Božidara Cvetković, Robert Szeklicki, Vito Janko, Przemyslaw Lutomski, Mitja Luštrek, **Real-time activity monitoring with a wristband and a smartphone**,
Information Fusion, vol. 43, 2018, pp. 77-93,
https://doi.org/10.1016/j.inffus.2017.05.004



**For additional information, please contact boza.cvetkovic@gmail.com**

**For other papers visit www.bozacvetkovic.net**

 
